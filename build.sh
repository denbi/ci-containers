#!/bin/bash
for f in $(find . -maxdepth 1 -type d -not -path '*/\.*' -not -path . -printf '%P\n'); do
  echo "Building and pushing ${f}"
  docker build ${f} -t gitlab.ub.uni-bielefeld.de:4567/denbi/ci-containers/${f};
  docker push gitlab.ub.uni-bielefeld.de:4567/denbi/ci-containers/${f};
done